# Multiple tools for api-fast

import os
from configparser import ConfigParser

config = ConfigParser()
HOME_DIR = os.getenv('HOME')


def get_config():
    default_dir = os.path.join(HOME_DIR, '.tryton')
    config_file = os.path.join(default_dir, 'config_pos.ini')
    config = ConfigParser()
    config.read(config_file)
    return config
