# CONNECTION PARAMS
import os
import ssl
import logging

log = logging.getLogger(__name__)

env = os.environ
DB = env['DB_NAME']
USER = env['DB_USER']
HOST = env['DB_HOST']
PASSWD = env['DB_PASS']

ENV = env['FASTENV']
SECRET = env['SECRET']

log.info(f"Enviroment: {ENV} ")

protocol = 'wss'
ssl_context = ssl._create_unverified_context()
if ENV == 'development':
    protocol = 'ws'
    ssl_context = None

ws_server = f"{protocol}://localhost:5090/ws"
