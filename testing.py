import requests
import simplejson as json

# The port is 7001 by default
api_url = 'http://localhost:7001'


ctx = {
    'company': 'UBUNTU INC',
    'sale_device': 'CAJA-10',
    'shop': 'Shop Wall Boulevard',
    'street': 'Cll 21 # 172-81. Central Park',
    'user': 'Charles Chapplin',
    'city': 'Dallas',
    'zip': '0876',
    'phone': '591 5513 455',
    'id_number': '123456789-0',
    'tax_regime': 'NA',
    'barcode': 'Z234'
}

device = {
    'interface': 'network',
    'device': '192.168.0.10',
    'profile': 'TM-P80',
}


def test_print():
    route = api_url + '/test_print'
    # dont pass attribute device for read config file config_pos.ini
    body = {
        'context': ctx,
        # 'device': device,
    }
    return route, body


if __name__ == "__main__":
    route, body = test_print()

    if body:
        data = json.dumps(body)
        result = requests.post(route, data=data)
    else:
        result = requests.get(route)

    print(result.status_code, 'result')
    print(result.text, 'result')
