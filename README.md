# First install dependences

pip3 install -U uvicorn uvloop python-dotenv ujson starlette fastapi aiofiles


# For local test
uvicorn main:app --reload --port 7001 --root-path /api


# For production https

uvicorn main:app --reload --host 0.0.0.0 --port 7001 --env-file .env-config --ssl-keyfile=/home/psk/.me_certificate/privkey.pem --ssl-certfile=/home/psk/.me_certificate/fullchain.pem  

# for set config printer
set file config_pos.ini in source /home/(user)/.tryton/
```
[General]
#########################################
# Printer Interface and Printer name
# For Unix use eg:
#	usb,/dev/usb/lp0
#	network,192.168.0.36
#	cups,EPSON-TM-T20
#
# For Windows use just name printer eg:
#   usb,SATPOS
#
##########################################
printer_sale_name=network,192.168.1.33
profile_printer=TM-P80
```
